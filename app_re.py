import socket
import random


def random_url():
    url_list = ["HTTP/1.1 301 Moved Permanently\r\n"
                + "Location: http://gsyc.es\r\n\r\n",
                "HTTP/1.1 301 Moved Permanently\r\n"
                + "Location: http://www.google.com\r\n\r\n",
                "HTTP/1.1 301 Moved Permanently\r\n"
                + "Location: https://gsyc.urjc.es // blog.html\r\n\r\n"
                ]
    length = len(url_list) - 1
    n = random.randint(0, length)
    return url_list[n]


S = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

S.bind(('localhost', 2002))

S.listen(5)


def serve_url(url):
    try:
        while True:
            print("Waiting connection...")
            (recvSocket, address) = S.accept()
            received = recvSocket.recv(2048)
            print(received)
            recvSocket.send(url.encode())
            recvSocket.close()
    except KeyboardInterrupt:
        S.close()


if __name__ == '__main__':
    rand_url = random_url()
    print("url = ", rand_url)
    serve_url(rand_url)
